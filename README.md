# NodejsChallenge

NodejsChallenge

## Start dev mode
```
docker-compose up
yarn
yarn start:dev
```
## Seed
```
npx ts-node ./node_modules/.bin/typeorm migration:run
```
## Env
```
NODE_ENV=development
PORT=4000
DB_USERNAME=root
DB_PASSWORD=root
DB_NAME=app
```
