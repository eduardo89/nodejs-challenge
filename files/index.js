const fs = require('fs');
const readline = require('readline');
const path = require('path');
const readStream = fs.createReadStream(path.join(__dirname, process.argv[2]));
const rl = readline.createInterface(readStream);

const dic = {}
let numCharRepeated = 0;
let wordsRepeated = 0; 

rl.on('line', function(line) {
  const words = line.replace(/[^a-zA-Z\d\s:]/g, '').split(' ');
  words.forEach((word) => {
    const lword = word.toLowerCase();
    if (dic[lword]) {
      dic[lword]++;
      numCharRepeated += lword.length;
      wordsRepeated++;
    } else {
      dic[lword] = 1;
    }
  });
});

rl.on('close', function() {
  console.log(`Hubo un total de ${wordsRepeated.toLocaleString()} de texto repetido con una cantidad de ${numCharRepeated.toLocaleString()} de caracteres`);
});