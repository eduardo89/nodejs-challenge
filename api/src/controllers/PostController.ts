import type { Request, Response } from 'express';
import { PostFactory } from '../domain/factories'
import { GroupService } from '../services';
import { User, Group, Post } from '../domain/entities';
import { logger } from '../utils';

export class PostController {
  static index = async (req: Request, res: Response) => {
    const groupId = parseInt(req.params.id, 10);
    const postsDB = await Post.createQueryBuilder("post")
      .select(["post", "user"])
      .innerJoin("post.author", "user")
      .innerJoin("post.group", "group")
      .where("group.id = :groupId", { groupId })
      .getMany();
    const posts = postsDB.map(post => PostFactory.make(post));
    res.send(posts);
  };

  static create = async (req: Request, res: Response) => {
    const groupId = parseInt(req.params.id, 10);
    const { userId } = res.locals;
    const { title, content } = req.body;

    try {
      const post = PostFactory.make({ title, content });
      const postCreated = await GroupService.createPost(groupId, userId, post);
      res.send(postCreated);
    } catch (error) {
      logger.error('', error);
      res.status(500).send();
    }
  };

  static update = async (req: Request, res: Response) => {
    const groupId = parseInt(req.params.groupId, 10);
    const postId = parseInt(req.params.postId, 10);
    const { userId } = res.locals;
    const { title, content } = req.body;

    try {
      const post = PostFactory.make({ id: postId, title, content });
      const postCreated = await GroupService.updatePost(groupId, userId, post);
      res.send(postCreated);
    } catch (error) {
      logger.error('', error);
      res.status(500).send();
    }
  };
}