import type { Request, Response } from 'express';
import { UserFactory } from '../domain/factories'
import { UserService } from '../services';
import { User } from '../domain/entities';
import { createJWT, logger } from '../utils';

export class AuthenticationController {
  static login = async (req: Request, res: Response) => {
    const { email, password } = req.body;
    let user: User;

    try {
      user = await User.findOneOrFail({ where: { email } });
    } catch (error) {
      res.status(401).send();
      return;
    }
    
    if (!user.checkValidPassword(password)) {
      res.status(401).send();
      return;
    }

    const token = createJWT({ id: user.id, fullName: user.fullName, email: user.email });
    res.send({ token });
  };

  static async register (req: Request, res: Response) {
    const { fullName, role, email, password } = req.body;

    const user = UserFactory.make({
      fullName,
      role,
      email,
      password,
    });

    try {
      const newUser = await UserService.saveUser(user);
      res.send(newUser);
    } catch (error) {
      logger.error('', error);
      res.status(500).send();
    }
  }
}