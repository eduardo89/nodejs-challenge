import type { Request, Response } from 'express';
import { GroupFactory } from '../domain/factories'
import { GroupService } from '../services';
import { Group, User } from '../domain/entities';
import { logger } from '../utils';

export class GroupController {
  static index = async (req: Request, res: Response) => {
    const groupsBD = await Group.find({relations: ["members"]});
    const groups = groupsBD.map(group => GroupFactory.make(group));
    res.send(groups);
  };

  static create = async (req: Request, res: Response) => {
    const { name } = req.body;
    const group = GroupFactory.make({name});
    await group.save();
    res.send(group);
  };

  static addMember = async (req: Request, res: Response) => {
    const groupId = parseInt(req.params.id, 10);
    const { userId } = res.locals;

    try {
      const group = await GroupService.addNewMember(groupId, userId);
      res.send(group);
    } catch (error) {
      logger.error('', error);
      res.status(500).send();
    }
  }
}