import { User, Post } from '.';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToMany,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";

export interface GroupInterface {
  id?: number;
  name: string;
  members?: User[];
  posts?: Post[];
  createdAt?: Date;
  updatedAt?: Date;
};

@Entity()
export class Group extends BaseEntity implements GroupInterface {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @ManyToMany(type => User, user => user.groups)
  members: User[];

  @OneToMany(type => Post, post => post.group)
  posts: Post[];

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}