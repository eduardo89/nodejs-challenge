import { Group, Post, Comment } from '.';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToMany,
  JoinTable,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";
import * as bcrypt from "bcryptjs";
import { MaxLength, Length, IsEmail, IsNotEmpty } from "class-validator";

export enum UserRole {
  ADMIN = 'ADMIN',
  USER = 'USER',
}

export interface UserInterface {
  id?: number;
  fullName: string;
  role: UserRole;
  email: string;
  password?: string;
  groups?: Group[];
  posts?: Post[];
  comments?: Comment[];
  createdAt?: Date;
  updatedAt?: Date;
};

@Entity()
export class User extends BaseEntity implements UserInterface {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @IsNotEmpty()
  @MaxLength(125)
  fullName: string;

  @Column()
  @IsNotEmpty()
  @MaxLength(30)
  role: UserRole;

  @Column({unique: true})
  @IsNotEmpty()
  @IsEmail()
  @MaxLength(125)
  email: string;

  @Column()
  @Length(6, 30)
  password: string;

  @ManyToMany(type => Group, group => group.members)
  @JoinTable()
  groups: Group[];

  @OneToMany(type => Post, post => post.author)
  posts: Post[];

  @OneToMany(type => Comment, comment => comment.author)
  comments: Comment[];

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;

  hashPassword() {
    this.password = bcrypt.hashSync(this.password, 8);
  };

  checkValidPassword(password: string) {
    return bcrypt.compareSync(password, this.password);
  };
}