import { Post, User } from '.';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";

export interface CommentInterface {
  id?: number;
  content: string;
  post?: Post;
  author?: User;
  createdAt?: Date;
  updatedAt?: Date;
};

@Entity()
export class Comment extends BaseEntity implements CommentInterface  {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  content: string;

  @ManyToOne(type => Post, post => post.comments)
  post: Post;

  @ManyToOne(type => User, user => user.comments)
  author: User;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}