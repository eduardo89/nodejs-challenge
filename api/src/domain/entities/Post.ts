import { Group, User, Comment } from '.';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToOne,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from "typeorm";


export interface PostInterface {
  id?: number;
  title: string;
  content: string;
  group?: Group;
  author?: User;
  comments?: Comment[];
  createdAt?: Date;
  updatedAt?: Date;
};

@Entity()
export class Post extends BaseEntity implements PostInterface {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  content: string;

  @ManyToOne(type => Group, group => group.posts)
  group: Group;

  @ManyToOne(type => User, user => user.posts)
  author: User;

  @OneToMany(type => Comment, comment => comment.post)
  comments: Comment[];

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;
}