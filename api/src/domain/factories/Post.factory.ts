import { Post, PostInterface } from '../entities';
import { GroupFactory, UserFactory, CommentFactory } from '.';

export class PostFactory  {
  static make(data: PostInterface): Post {
    const { id, title, group, author, comments, content, updatedAt, createdAt } = data;
    const post = new Post();
    post.id = id;
    post.title = title;
    post.content = content;
    post.updatedAt = updatedAt;
    post.createdAt = createdAt;
    post.group = group ? GroupFactory.make(group) : null;
    post.author = author ? UserFactory.make(author) : null;
    post.comments = comments ? comments.map(CommentFactory.make) : [];
    return post;
  };
}