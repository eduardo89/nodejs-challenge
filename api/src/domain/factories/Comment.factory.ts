import { Comment, CommentInterface } from '../entities';
import { PostFactory, UserFactory } from '.';

export class CommentFactory  {
  static make(data: CommentInterface): Comment {
    const { id, content, post, author, updatedAt, createdAt } = data;
    const comment = new Comment();
    comment.id = id;
    comment.content = content;
    comment.updatedAt = updatedAt;
    comment.createdAt = createdAt;
    comment.post = post ? PostFactory.make(post) : null;
    comment.author = author ? UserFactory.make(author) : null;
    return comment;
  };
}