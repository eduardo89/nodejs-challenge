import { User, UserInterface } from '../entities';
import { GroupFactory, PostFactory, CommentFactory } from '.';

export class UserFactory  {
  static make(data: UserInterface): User {
    const { id, fullName, email, password, groups, role, posts, comments, updatedAt, createdAt } = data;
    const user = new User();
    user.id = id;
    user.fullName = fullName;
    user.role = role;
    user.email = email;
    user.password = password;
    user.updatedAt = updatedAt;
    user.createdAt = createdAt;
    user.groups = groups ? groups.map(GroupFactory.make) : [];
    user.posts = posts ? posts.map(PostFactory.make) : [];
    user.comments = comments ? comments.map(CommentFactory.make) : [];
    return user;
  };
}