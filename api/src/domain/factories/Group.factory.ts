import { Group, GroupInterface } from '../entities';
import { PostFactory, UserFactory } from '.';

export class GroupFactory  {
  static make(data: GroupInterface): Group {
    const { id, name, members, posts, updatedAt, createdAt } = data;
    const group = new Group();
    group.id = id;
    group.name = name;
    group.updatedAt = updatedAt;
    group.createdAt = createdAt;
    group.members = members ? members.map(UserFactory.make) : [];
    group.posts = posts ? posts.map(PostFactory.make) : [];
    return group;
  };
}