import * as dotenv from "dotenv";

dotenv.config();

export const appConfig = {
  PORT: process.env.PORT || 3000,
  DB_NAME: process.env.DB_NAME,
  DB_USERNAME: process.env.DB_USERNAME,
  DB_PASSWORD: process.env.DB_PASSWORD,
  DB_PORT: 5432,
  JWT_SECRET: 'GRS6Mhk7vS2BOmpFzG8d0wKY87GZ_6mqpygbm94FPCHf4-t4EnFd',
  JWT_EXPIRES_IN: '8h',
}
