import path from 'path';
import fs from 'fs';
import appRoot from 'app-root-path';
import { format, transports, createLogger } from 'winston';

const logDirectory = path.resolve(`${appRoot}`, "logs");
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

var options = {
  infofile: {
    level: "info",
    filename: path.resolve(logDirectory, "info.log"),
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5
  },
  errorfile: {
    level: "error",
    filename: path.resolve(logDirectory, "error.log"),
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 5
  },
  console: {
    handleExceptions: true,
    format: format.combine(
      format.timestamp(),
      format.colorize(),
      format.simple()
    ),
  }
};

export const logger = createLogger({
  format: format.combine(
    format.timestamp(),
    format.json(),
    
  ),
  level: 'debug',
  transports: [
    new transports.File(options.infofile),
    new transports.File(options.errorfile),
    new transports.Console(options.console)
  ]
});
