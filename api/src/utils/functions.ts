import { appConfig } from "./config";
import * as jwt from "jsonwebtoken";

export const createJWT = (body: any): string => {
  return jwt.sign(
    body,
    appConfig.JWT_SECRET,
    { expiresIn: appConfig.JWT_EXPIRES_IN }
  );
}