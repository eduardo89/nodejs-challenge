import {MigrationInterface, QueryRunner} from "typeorm";
import { UserFactory } from "../../domain/factories";
import { UserRole } from "../../domain/entities";

export class seedAdmin1592967485972 implements MigrationInterface {
    name = 'seedAdmin1592967485972'

    public async up(queryRunner: QueryRunner): Promise<void> {
        const user = UserFactory.make({
            email: 'eduardo@tk.com',
            fullName: 'Eduardo Gamarra',
            password: '123456789',
            role: UserRole.ADMIN,
        });
        user.hashPassword();
        await queryRunner
            .manager
            .createQueryBuilder()
            .insert()
            .into("user")
            .values(user)
            .execute()
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ALTER COLUMN "id" DROP DEFAULT`);
        await queryRunner.query(`DROP SEQUENCE "user_id_seq"`);
    }

}
