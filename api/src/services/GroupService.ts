import { User, Group, Post } from '../domain/entities';
import { GroupFactory, PostFactory } from '../domain/factories';

export class GroupService {
  static addNewMember = async (groupId: number, userId: number): Promise<Group> => {
    let group = await Group.createQueryBuilder("group")
      .innerJoin("group.members", "user")
      .where("user.id = :userId", { userId })
      .getOne();

    if (group) {
      throw new Error('User is already in the group.');
    } else {
      group = await Group.findOneOrFail(groupId);
    }

    const user = await User.findOneOrFail(userId, { relations: ["groups"] });
    user.groups.push(group);
    await user.save();
    return group;
  }

  static createPost = async (groupId: number, userId: number, postData: Post): Promise<Post> => {
    const group = await Group.createQueryBuilder("group")
      .select(["group", "user"])
      .innerJoin("group.members", "user")
      .where("user.id = :userId", { userId })
      .andWhere("group.id = :groupId", { groupId })
      .getOne();

    if (!group) {
      throw new Error('User is not a member of the group');
    }

    postData.group = group;
    postData.author = group.members.shift();
    postData.save();
    return postData;
  }

  static updatePost = async (groupId: number, userId: number, postData: Post): Promise<Post> => {
    const group = await Group.createQueryBuilder("group")
      .select(["group", "user"])
      .innerJoin("group.members", "user")
      .where("user.id = :userId", { userId })
      .andWhere("group.id = :groupId", { groupId })
      .getOne();

    if (!group) {
      throw new Error('User is not a member of the group');
    }

    const post = await Post.createQueryBuilder("post")
      .select(["post", "user"])
      .innerJoin("post.author", "user")
      .where("user.id = :userId", { userId })
      .andWhere("post.id = :postId", { postId: postData.id })
      .getOne();

    const { title, content } = postData;

    post.title = title;
    post.content = content;
    post.save();
    return post;
  }
}