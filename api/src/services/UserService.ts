import { User } from '../domain/entities';

export class UserService {
  static saveUser = async (newUser: User): Promise<User> => {
    const userFound = User.findOne({ where: {
      email: newUser.email,
    }});

    if (userFound) {
      throw new Error('Email already exists');
    }
    newUser.hashPassword();
    await newUser.save();
    return newUser;
  }
}