import type express from 'express';
import { AuthenticationController, GroupController, PostController } from './controllers';
import { validateJWT } from './middlewares/validateJWT';
import { validateRole } from './middlewares/validateRole';
import { UserRole } from './domain/entities';

export const routes = (router: express.Router) => {
  router.post('/login', AuthenticationController.login);
  router.post('/register', validateJWT, validateRole([UserRole.ADMIN]), AuthenticationController.register);

  router.post('/group', validateJWT, validateRole([UserRole.ADMIN]),GroupController.create);

  router.post('/group/:id/user', validateJWT, GroupController.addMember);
  router.post('/group/:id/post', validateJWT, PostController.create);
  router.put('/group/:groupId/post/:postId', validateJWT, PostController.update);

  router.get('/group', validateJWT, validateRole([UserRole.ADMIN]), GroupController.index);
  router.get('/group/:id/post', validateJWT, PostController.index);

  return router;
}