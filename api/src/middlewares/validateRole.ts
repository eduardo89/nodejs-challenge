import type { Request, Response, NextFunction} from 'express';
import { User } from '../domain/entities';

export const validateRole = (roles: string[]) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    const userId = res.locals.userId;

    let user: User;
    try {
      user = await User.findOneOrFail(userId);
      console.log('found!!', user);
    } catch (id) {
      res.status(401).send();
    }

    if (roles.indexOf(user.role) > -1) next();
    else res.status(401).send();
  };
};