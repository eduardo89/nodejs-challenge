import type { Request, Response, NextFunction} from 'express';
import * as jwt from "jsonwebtoken";
import { createJWT } from "../utils/functions";
import { appConfig } from "../utils/config";

export const validateJWT = (req: Request, res: Response, next: NextFunction) => {
  const tokenHeader = <string>req.headers["authorization"];
  let jwtPayload: any;
  try {
    const token = tokenHeader.split(' ')[1];
    jwtPayload = jwt.verify(token, appConfig.JWT_SECRET);
  } catch (error) {
    console.log('error JWT', error);
    res.status(401).send();
    return;
  }
  const { id, fullName, email } = jwtPayload;
  res.locals.userId = id;
  const newToken = createJWT({ id, email, fullName });

  res.setHeader("token", newToken);
  next();
};