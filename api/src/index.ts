import 'reflect-metadata';
import express from 'express';
import morgan from 'morgan';
import {appConfig, logger } from './utils';
import { routes } from './routes';
import { createConnection } from 'typeorm';

createConnection().then(connection => {
  const app: express.Application = express();
  app.use(express.json());
  app.use(morgan('combined', { stream: { write: logger.info.bind(logger) }}))

  app.use('/api', routes(express.Router()));

  app.use(function(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
    logger.error('', err);
    res.status(err.status || 500).send({
      error: 'Internal server error'
    });
  });

  app.listen(appConfig.PORT, function () {
    console.log(`Server is listening on port ${appConfig.PORT}`);
  });
}).catch(error => console.log(error));