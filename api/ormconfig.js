const dotenv = require("dotenv");
dotenv.config();

module.exports = {
  type: "postgres",
  host: process.env.BD_HOST || "localhost",
  port: process.env.DB_PORT || 5432,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  synchronize: true,
  logging: false,
  entities: [
    "src/domain/entities/**/*.ts"
  ],
  migrations: [
    "src/database/migration/**/*.ts"
  ],
  subscribers: [
    "src/subscriber/**/*.ts"
  ],
  cli: {
    migrationsDir: "src/database/migration"
  },
};